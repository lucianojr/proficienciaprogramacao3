public abstract class Cypher
{
    protected final char[] alphabet = {
            'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm',
            'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z'
    };
    private char[] replacements;

    public abstract char[] generateReplacementsTable();

    public abstract String encode(String input);

    public abstract String decode(String input);
}