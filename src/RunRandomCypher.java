public class RunRandomCypher {
    private static String inputFilePath;
    private static String outputFilePath;
    private static String key;
    private static char option;

    /**
     * @param args
     */
    public static void main(String[] args) {
        option = args[0].charAt(1);
        key = args[1];
        inputFilePath = args[2];
        outputFilePath = args[3];

        RandomCypher randomCypher = new RandomCypher(key);
        randomCypher.setInputFilePath(inputFilePath);
        randomCypher.setOutputFilePath(outputFilePath);

        if (option == 'k') {
            randomCypher.encode();
        } else if (option == 'd') {
            randomCypher.decode();
        }


    }
}
