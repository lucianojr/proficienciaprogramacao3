public class RunCaesarCypher {

    private static String inputFilePath;
    private static String outputFilePath;
    private static int key;
    private static char option;

    /**
     * @param args
     */
    public static void main(String[] args) {
        option = args[0].charAt(1);
        key = Integer.valueOf(args[1]);
        inputFilePath = args[2];
        outputFilePath = args[3];

        CaesarCypher caesarCypher = new CaesarCypher(key);
        caesarCypher.setInputFilePath(inputFilePath);
        caesarCypher.setOutputFilePath(outputFilePath);

        if (option == 'e') {
            caesarCypher.encode();
        } else if (option == 'd') {
            caesarCypher.decode();
        }


    }
}
