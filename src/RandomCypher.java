import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

public class RandomCypher extends Cypher {
    private int key;
    private String inputFilePath;
    private String outputFilePath;
    private char[] alphabet;


    public RandomCypher(String key) {
        char[] caracters = key.toCharArray();
        int count = caracters.length;
        for (int i = 0; i < caracters.length; i++) {
            for (int j = 0; j < i - 1; j++) {
                if(Character.toLowerCase(caracters[i]) == Character.toLowerCase(caracters[j])){
                    caracters[j] = ' ';
                    count --;
                }
            }
        }

        char[] caratersSanitize = new char[count];
        int j =0;
        for (int i = 0; i < caracters.length; i++) {
            if(caracters[i] != ' '){
                caratersSanitize[j] = caracters[i];
                j ++;
            }
        }

        this.alphabet = new char[26];
        for (int i = 0; i < caratersSanitize.length; i++) {
            alphabet[i] = Character.toLowerCase(caratersSanitize[i]);
        }

        j = 0;
        boolean jatem = false;
        for (int i = super.alphabet.length -1; i >= 0; i--) {
            jatem = false;
            for (int k = 0; k < caratersSanitize.length; k++) {
                if(super.alphabet[i] == Character.toLowerCase(caratersSanitize[k])) {
                    jatem = true;
                }
            }
            if(!jatem) {
                alphabet[caratersSanitize.length + j] = super.alphabet[i];
                j++;
            }
        }
    }

    public int getKey() {
        return key;
    }

    public void setKey(int key) {
        this.key = key;
    }

    public String getInputFilePath() {
        return inputFilePath;
    }

    public void setInputFilePath(String inputFilePath) {
        this.inputFilePath = inputFilePath;
    }

    public String getOutputFilePath() {
        return outputFilePath;
    }

    public void setOutputFilePath(String outputFilePath) {
        this.outputFilePath = outputFilePath;
    }

    public void encode(){
        try {
            String text = new Scanner(new File(inputFilePath)).useDelimiter("\\Z").next();
            String output = "";
            String [] words = text.split(" ");

            for (String word : words) {
                output += encode(word)+" ";
            }

            writeFile(output);
        } catch (IOException e) {
            System.err.printf("Erro na abertura do arquivo: %s.\n", e.getMessage());
        }
    }

    public void decode(){
        try {
            String text = new Scanner(new File(inputFilePath)).useDelimiter("\\Z").next();
            String output = "";
            String [] words = text.split(" ");

            for (String word : words) {
                output += decode(word)+" ";
            }

            writeFile(output);
        } catch (IOException e) {
            System.err.printf("Erro na abertura do arquivo: %s.\n", e.getMessage());
        }
    }

    public void writeFile(String content) throws IOException {
        BufferedWriter buffWrite = new BufferedWriter(new FileWriter(outputFilePath));
        buffWrite.append(content);
        buffWrite.close();
    }

    @Override
    public char[] generateReplacementsTable() {
        return new char[0];
    }

    @Override
    public String decode(String input) {
        char sequence[] = input.toCharArray();
        char result [] = new char[sequence.length];
        int indexAux = -1;
        for (int i = 0; i < sequence.length; i++) {
            indexAux = getThisAlphabetIndex(sequence[i]);
            if (sequence[i] == '\n') {
                result[i] = '\n';
            } else if (indexAux == -1) {
                result[i] = ' ';
            } else {
                result[i] = super.alphabet[indexAux];
            }

        }
        return new String(result);
    }

    @Override
    public String encode(String input) {
        char sequence[] = input.toCharArray();
        char result [] = new char[sequence.length];
        int indexAux = -1;
        for (int i = 0; i < sequence.length; i++) {
            indexAux = getAlphabetIndex(sequence[i]);
            if (sequence[i] == '\n') {
                result[i] = '\n';
            } else if (indexAux == -1) {
                result[i] = ' ';
            } else {
                result[i] = alphabet[indexAux];
            }

        }
        return new String(result);
    }

    private int getAlphabetIndex(char c){
        for (int i = 0; i < alphabet.length; i++) {
            if(super.alphabet[i] == Character.toLowerCase(c)){
                return i;
            }
        }

        return -1;
    }

    private int getThisAlphabetIndex(char c){
        for (int i = 0; i < alphabet.length; i++) {
            if(alphabet[i] == Character.toLowerCase(c)){
                return i;
            }
        }

        return -1;
    }
}
