import java.io.*;
import java.util.Scanner;

public class CaesarCypher extends Cypher {
    private int key;
    private String inputFilePath;
    private String outputFilePath;

    public CaesarCypher(int key) {
        this.key = key;
    }

    public int getKey() {
        return key;
    }

    public void setKey(int key) {
        this.key = key;
    }

    public String getInputFilePath() {
        return inputFilePath;
    }

    public void setInputFilePath(String inputFilePath) {
        this.inputFilePath = inputFilePath;
    }

    public String getOutputFilePath() {
        return outputFilePath;
    }

    public void setOutputFilePath(String outputFilePath) {
        this.outputFilePath = outputFilePath;
    }

    public void encode(){
        try {
            String text = new Scanner(new File(inputFilePath)).useDelimiter("\\Z").next();
            String output = "";
            String [] words = text.split(" ");

            for (String word : words) {
                output += encode(word)+" ";
            }

            writeFile(output);
        } catch (IOException e) {
            System.err.printf("Erro na abertura do arquivo: %s.\n", e.getMessage());
        }
    }

    public void decode(){
        try {
            String text = new Scanner(new File(inputFilePath)).useDelimiter("\\Z").next();
            String output = "";
            String [] words = text.split(" ");

            for (String word : words) {
                output += decode(word)+" ";
            }

            writeFile(output);
        } catch (IOException e) {
            System.err.printf("Erro na abertura do arquivo: %s.\n", e.getMessage());
        }
    }

    public void writeFile(String content) throws IOException {
        BufferedWriter buffWrite = new BufferedWriter(new FileWriter(outputFilePath));
        buffWrite.append(content);
        buffWrite.close();
    }

    @Override
    public char[] generateReplacementsTable() {
        return new char[0];
    }

    @Override
    public String decode(String input) {
        char sequence[] = input.toCharArray();
        char result [] = new char[sequence.length];
        int indexAux = -1;
        for (int i = 0; i < sequence.length; i++) {
            indexAux = getAlphabetIndex(sequence[i]);
            if (sequence[i] == '\n') {
                result[i] = '\n';
            } else if (indexAux == -1) {
                result[i] = ' ';
            } else if(indexAux - key >= 0) {
                result[i] = alphabet[indexAux - key];
            } else {
                result[i] = alphabet[alphabet.length - (key - indexAux)];
            }

        }
        return new String(result);
    }

    @Override
    public String encode(String input) {
        char sequence[] = input.toCharArray();
        char result [] = new char[sequence.length];
        int indexAux = -1;
        for (int i = 0; i < sequence.length; i++) {
            indexAux = getAlphabetIndex(sequence[i]);
            System.out.println(indexAux+" + "+key+" -----"+alphabet.length);
            if (sequence[i] == '\n') {
                result[i] = '\n';
            } else if (indexAux == -1) {
                result[i] = ' ';
            } else if(indexAux + key < alphabet.length) {
                result[i] = alphabet[indexAux + key];
            } else {
                result[i] = alphabet[(indexAux + key) - alphabet.length];
            }

        }
        return new String(result);
    }

    private int getAlphabetIndex(char c){
        for (int i = 0; i < alphabet.length; i++) {
            if(alphabet[i] == Character.toLowerCase(c)){
                return i;
            }
        }

        return -1;
    }
}
